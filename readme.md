Host: http://18.136.235.184:8080

### Generating Object
`[GET]` : /

| Param | Content |
| ------ | ------ |
| BustGirth | Integer |
| WaistGirth | Integer |
| HipGirth | Integer |
| Height | Integer |
| NeckGirth | Integer |
| ChestWidth | Integer |
| BustToBust | Integer |
| ShoulderWidth | Integer |
| BackWidth | Integer |
| WaistLength | Integer |
| ArmholeGirthLeft | Integer |
| BicepGirth | Integer |
| ShoulderSlope | Integer |
| Type | Integer |
| FrontLength | Integer |
| SleeveLength | Integer |
| CuffSize | Integer |
| Fit | Integer |
| ShortSleeve | Integer |

#### Response
##### Success
```
{
	status: 'success',
	data: {
		avatar: 'http://18.136.235.184:8080/renderer/demosc_Avatar.obj',
		garment: 'http://18.136.235.184:8080/renderer/demosc_0.obj',
	}
	updated_at: 1554566926,
}
```
##### Error
```
{
	status: 'error',
	message: 'error message',
	updated_at: 1554566926,
}
```

#### Sample
http://18.136.235.184:8080/?BustGirth=90&WaistGirth=75&HipGirth=90&Height=180&NeckGirth=36&ChestWidth=40&BustToBust=20&ShoulderWidth=40&BackWidth=41&WaistLength=38&ArmholeGirthLeft=42&BicepGirth=30&ShoulderSlope=1&Type=SHIRT&FrontLength=72&SleeveLength=55&CuffSize=18&Fit=1&ShortSleeve=0&
