const express = require('express')
const app = express()
const port = 8080

const util = require('util');
const exec = util.promisify(require('child_process').exec);

const defaultSchema = [
	'BustGirth',
	'WaistGirth',
	'HipGirth',
	'Height',
	'NeckGirth',
	'ChestWidth',
	'BustToBust',
	'ShoulderWidth',
	'BackWidth',
	'WaistLength',
	'ArmholeGirthLeft',
	'BicepGirth',
	'ShoulderSlope',
	'Type',
	'FrontLength',
	'SleeveLength',
	'CuffSize',
	'Fit',
	'ShortSleeve',
]

function now() {
	return (new Date().getTime()) / 1000
}

async function execCommand(qy) {
	try {
			let command = 'LD_LIBRARY_PATH=/home/ubuntu/3d3_app/renderer/3D3libs '
			command += `/home/ubuntu/3d3_app/renderer/3D3 demosc 1 DimBegin `
			command += `BustGirth ${qy.BustGirth} `
			command += `WaistGirth ${qy.WaistGirth} `
			command += `HipGirth ${qy.HipGirth} `
			command += `Height ${qy.Height} `
			command += `NeckGirth ${qy.NeckGirth} `
			command += `ChestWidth ${qy.ChestWidth} `
			command += `BustToBust ${qy.BustToBust} `
			command += `ShoulderWidth ${qy.ShoulderWidth} `
			command += `BackWidth ${qy.BackWidth} `
			command += `WaistLength ${qy.WaistLength} `
			command += `ArmholeGirthLeft ${qy.ArmholeGirthLeft} `
			command += `BicepGirth ${qy.BicepGirth} `
			command += `ShoulderSlope ${qy.ShoulderSlope} `
			command += `DimEnd StyleBegin `
			command += `Type ${qy.Type} `
			command += `FrontLength ${qy.FrontLength} `
			command += `SleeveLength ${qy.SleeveLength} `
			command += `CuffSize ${qy.CuffSize} `
			command += `Fit ${qy.Fit} `
			command += `ShortSleeve ${qy.ShortSleeve} `
			command += `StyleEnd`

			console.log(`executing command: ${command}`)

  		let { stdout, stderr } = await exec(command);
			// if (stderr && stderr.length > 0) {
			// 	console.log(stderr);
			// 	throw new Error(stderr);
			// }

			// console.log('stdout:', stdout);
	} catch (e) {
		throw new Error(e);
	}
}

function checkSchema(query, schema) {
	Object.keys(query).map(key => {
		const index = schema.indexOf(key)
		if (index > -1) {
			schema.splice(index, 1)
		}
	})
	return schema;
}

function missingSchema(res, schema) {
	return res.send({
		status: 'error',
		data: { schema },
		message: 'Missing parameters',
		updated_at: now(),
	});
}

async function renderer(req, res) {

	const missingSchma = checkSchema(req.query, defaultSchema)

	if (missingSchma.length > 0) return missingSchema(res, missingSchma)

	try {
		await execCommand(req.query);
		res.send({
			status: 'success',
			data: {
					avatar: 'http://18.136.235.184:8080/renderer/demosc_Avatar.obj',
					garment: 'http://18.136.235.184:8080/renderer/demosc_0.obj',
			},
			updated_at: now(),
		})
	} catch (e) {
		res.send({
			status: 'error',
			message: e.toString() + '234',
			updated_at: now(),
		})
	}
}


app.get('/', renderer)
app.use('/renderer', express.static('renderer'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
